module "argocd" {
  source = "./module/argocd"
  count  = 1
  tags = {
    Name       = "Argocd"
    Created_by = "terraform"
  }
}

module "argocd-image-updater" {
  source = "./module/argocd-image-updater"
  count  = 1
  tags = {
    Name       = "ArgoCdImageUpdater"
    Created_by = "terraform"
  }

  values_file = "values/argocd-image-updater-default-values.yaml"
}


