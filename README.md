# Infra

[ArgoCD1](https://www.youtube.com/watch?v=zGndgdGa1Tc)

[Terraform Helm Provider](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)

#### Terraform

```bash
terraform init
terraform apply --auto-approve
```

#### ArgoCD

```bash
kubectl get pods -n argocd
kubectl port-forward svc/argocd-server -n argocd 8080:80
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 --decode && echo
```

#### Docker Repository Tests

```bash
docker pull nginx:1.23.3
docker tag nginx:1.23.3 mahdad1988/nginx:v0.1.0
docker push mahdad1988/nginx:v0.1.0

docker tag nginx:1.23.3 mahdad1988/nginx-private:v0.1.0
docker push mahdad1988/nginx-private:v0.1.0
```

#### application-nginx-simple

```bash
kubectl apply -f example/application.yaml
```

#### application-nginx-simple-2

```bash
kubectl apply -f 2-example/application.yaml
```

#### application-nginx-private

```bash
kubectl apply -f 3-example/application.yaml
kubectl delete -f 3-example/application.yaml
# To access to gitlab for syncing the private project
ssh-keygen -t ed25519 -C "argocd@mahdad.me" -f ~/.ssh/argocd_ed25519
$ cat ~/.ssh/argocd_ed25519.pub
# add to https://gitlab.com/argocd1988/application-nginx-private/-/settings/repository#js-deploy-keys-settings
kubectl apply -f 3-example/git-repo-secret.yaml
kubectl apply -f 3-example/application.yaml
# To access for pulling image from docker hub
# got to hub.docker.com => your account => security => add new access token:
#   description: minikube
#   premission: raed-only
# To use the access token from your Docker CLI client:
# 1. Run docker login -u mahdad1988
# 2. At the password prompt, enter the personal access token.
# dckr_pat_VKlLqgq8kvzzu5dkMJnaFCrqGqU
kubectl create secret docker-registry dockerconfigjson -n foo-private \
    --docker-server="https://index.docker.io/v1/" \
    --docker-username=mahdad1988 \
    --docker-password=dckr_pat_VKlLqgq8kvzzu5dkMJnaFCrqGqU \
    --docker-email=info@mahdad.me
kubectl get secrets -n foo-private -o yaml
kubectl delete -f 3-example
```

#### Add metrics

```bash
kubectl apply -f 4-example/application.yaml
kubectl top pods -n kube-system
kubectl top node
kubectl delete -f 4-example/application.yaml
```

```bash
kubectl apply -f 5-example/git-repo-secret.yaml
kubectl apply -f 5-example/application.yaml
```

#### Image Updater Private 1

## Install argocd image updater with terraform

```bash
helm repo add argo https://argoproj.github.io/argo-helm
helm search repo argocd
kubectl get pods -n argocd
kubectl logs -f -l app.kubernetes.io/instance=argocd-image-updater -n argocd
```

## Repo

```bash
docker pull nginx:1.23.3
docker tag nginx:1.23.3 mahdad1988/nginx-updater1:v0.1.0
docker push mahdad1988/nginx-updater1:v0.1.0
```

### argocd mode or direct mode

```bash
# To access to gitlab for syncing the private project
ssh-keygen -t ed25519 -C "argocd@mahdad.me" -f ~/.ssh/argocd_ed25519_2
$ cat ~/.ssh/argocd_ed25519_2.pub
# add to https://gitlab.com/argocd1988/image-updater-private-1/-/settings/repository#js-deploy-keys-settings
kubectl apply -f 6-example/git-repo-secret.yaml
kubectl apply -f 6-example/application.yaml
```

### git mode

```bash
# To access to gitlab for syncing the private project
ssh-keygen -t ed25519 -C "argocd@mahdad.me" -f ~/.ssh/argocd_ed25519_write
$ cat ~/.ssh/argocd_ed25519_write.pub
# add to https://gitlab.com/argocd1988/image-updater-private-1/-/settings/repository#js-deploy-keys-settings
kubectl apply -f 7-example/git-repo-secret.yaml
kubectl apply -f 7-example/application.yaml
```

## Repo

```bash
docker tag nginx:1.23.3 mahdad1988/nginx-updater1:v0.3.1
docker push mahdad1988/nginx-updater1:v0.3.1
```

## Private Repo

```bash
docker tag nginx:1.23.3 mahdad1988/nginx-updater-private:v0.1.0
docker push mahdad1988/nginx-updater-private:v0.1.0
```

```bash
# docker hub access to staging namespace
kubectl create secret docker-registry dockerconfigjson -n staging \
    --docker-server="https://index.docker.io/v1/" \
    --docker-username=mahdad1988 \
    --docker-password=dckr_pat_VKlLqgq8kvzzu5dkMJnaFCrqGqU \
    --docker-email=info@mahdad.me
kubectl apply -f 8-example/application.yaml
```